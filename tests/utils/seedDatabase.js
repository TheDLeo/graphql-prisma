import prisma from '../../src/prisma'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'

const userOne = {
  input: {
    name: 'test',
    email: 'test@example.com',
    password: bcrypt.hashSync('Blue1234')
  },
  user: undefined,
  jwt: undefined
}

const userTwo = {
  input: {
    name: 'test2',
    email: 'test2@example.com',
    password: bcrypt.hashSync('Blue1234')
  },
  user: undefined,
  jwt: undefined
}

const postOne = {
  input: {
    title: 'Test post 1',
    body: 'Test body 1',
    published: true
  },
  post: undefined
}

const postTwo = {
  input: {
    title: 'Test post 2',
    body: 'Test body 2',
    published: false
  },
  post: undefined
}

const commentOne = {
  input: {
    text: 'Comment one text'
  },
  comment: undefined
}

const commentTwo = {
  input: {
    text: 'Comment two text'
  },
  comment: undefined
}

const seedDatabase = async () => {
  // clear all comments
  await prisma.mutation.deleteManyComments()
  // clear all posts
  await prisma.mutation.deleteManyPosts()
  // clear all users
  await prisma.mutation.deleteManyUsers()
  // create userOne
  userOne.user = await prisma.mutation.createUser({
    data: userOne.input
  })
  userOne.jwt = jwt.sign({ userId: userOne.user.id }, process.env.JWT_SECRET)
  
  // create userTwo
  userTwo.user = await prisma.mutation.createUser({
    data: userTwo.input
  })
  userTwo.jwt = jwt.sign({ userId: userTwo.user.id }, process.env.JWT_SECRET)
  
  // create post one
  postOne.post = await prisma.mutation.createPost({
    data: {
      ...postOne.input,
      author: {
        connect: {
          id: userOne.user.id
        }
      }
    }
  })

  // create post two
  postTwo.post = await prisma.mutation.createPost({
    data: {
      ...postTwo.input,
      author: {
        connect: {
          id: userOne.user.id
        }
      }
    }
  })

  // create comment one
  commentOne.comment = await prisma.mutation.createComment({
    data: {
      ...commentOne.input,
      author: {
        connect: {
          id: userTwo.user.id
        }
      },
      post: {
        connect: {
          id: postOne.post.id
        }
      }
    }
  })

  // create comment two
  commentTwo.comment = await prisma.mutation.createComment({
    data: {
      ...commentTwo.input,
      author: {
        connect: {
          id: userOne.user.id
        }
      },
      post: {
        connect: {
          id: postOne.post.id
        }
      }
    }
  })

}

export { seedDatabase as default, userOne, userTwo, postOne, postTwo, commentOne, commentTwo }
