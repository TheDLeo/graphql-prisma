import 'cross-fetch/polyfill'
import { gql } from 'apollo-boost'
import prisma from '../src/prisma'
import seedDatabase, { userOne } from './utils/seedDatabase'
import getClient from './utils/getClient'
import { createUser, getPosts, login, getProfile } from './utils/operations'

const client = getClient()

beforeEach(seedDatabase)

test('Should create a new user', async () => {
  const variables = {
    data: {
      name: 'Dan',
      email: 'dan@example.com',
      password: 'MyPass123'
    }
  }

  const response = await client.mutate({
    mutation: createUser,
    variables
  })

  const exists = await prisma.exists.User({ id: response.data.createUser.user.id })

  expect(exists).toBe(true)
})

test('Should retrieve only published posts for public users', async () => {
  const response = await client.query({ query: getPosts })

  expect(response.data.posts.length).toBe(1)
  expect(response.data.posts[0].published).toBe(true)
})

test('Should not login with bad credentials', async () => {
  const variables = {
    data: {
      email:'',
      password:''
    }
  }
  await expect(client.mutate({ mutation: login, variables })).rejects.toThrow()
})

test('Should not allow sign up with short password', async () => {
  const variables = {
    data: {
      name: 'Fail',
      email: 'fail@example.com',
      password: 'abc123'
    }

  }
  await expect(client.mutate({
    mutation: createUser,
    variables
  })).rejects.toThrow()
})

test('Should fetch user profile', async () => {
  const client = getClient(userOne.jwt)

  const { data } = await client.query({ query: getProfile })
  expect(data.me.id).toBe(userOne.user.id)
  expect(data.me.name).toBe(userOne.user.name)
  expect(data.me.email).toBe(userOne.user.email)

})
