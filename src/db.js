let users = [
  {
    id: '1',
    name: 'Dan',
    email: 'dan@example.com',
    age: 54
  },
  {
    id: '2',
    name: 'Lisa',
    email: 'lisa@example.com'
  },
  {
    id: '3',
    name: 'Mo',
    email: 'mo@example.com',
    age: 30
  }
];

let posts = [
  {
    id: 'a',
    title: 'post1',
    body: 'body1',
    published: true,
    author: '1'
  },
  {
    id: 'b',
    title: 'post2 a',
    body: 'body2 asdfasdfasfd',
    published: true,
    author: '2'
  },
  {
    id: 'c',
    title: 'post3 c',
    body: 'body3 sadf ahsihs oah psoh pohspoihd ',
    published: true,
    author: '2'
  }
];

let comments = [
  {
    id: 'abc123',
    text: 'hey this is really good',
    author: '1',
    post: 'a'
  },
  {
    id: 'abc124',
    text: 'hey this is really bad',
    author: '1',
    post: 'b'
  },
  {
    id: 'abc125',
    text: 'hey this really sucks',
    author: '3',
    post: 'b'
  },
  {
    id: 'abc126',
    text: 'hey this is really meh',
    author: '2',
    post: 'c'
  }
];

const db = {
  users,
  posts,
  comments
};

export { db as default };
