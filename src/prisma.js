import { Prisma } from 'prisma-binding';
import { fragmentReplacements } from './resolvers/index';

const prisma = new Prisma({
  typeDefs: 'src/generated/prisma.graphql',
  endpoint: process.env.PRISMA_ENDPOINT,
  secret: process.env.PRISMA_SECRET,
  fragmentReplacements
})

export { prisma as default}

// const createPostForUser = async (authorId, data) => {
//   const post = await prisma.mutation.createPost({
//     data: {
//       ...data,
//       author: {
//         connect: {
//           id: authorId
//         }
//       }
//     }
//   }, '{ id }')

//   const user = await prisma.query.user({
//     where: {
//       id: authorId
//     }
//   }, '{ id name email posts { id title published } }');

//   return user;
// }

// // createPostForUser('cjn2nvpqt001d0817f28ea94g', {
// //   title: "Great books to read",
// //   body: "I don't know of any",
// //   published: true
// // }).then((user) => {
// //   console.log(JSON.stringify(user, undefined, 2));
// // })

// const updatePostForUser = async (postId, data) => {
//   const post = await prisma.mutation.updatePost({
//     data,
//     where: {
//       id: postId
//     }
//   }, '{ author { id } }')

//   const user = await prisma.query.user({
//     where: {
//       id: post.author.id
//     }
//   }, '{ id name email posts { id title body published } }');

//   return user;
// }

// // updatePostForUser('cjn7jur3j00100909npad5hml', {
// //   body: "12 Rules for Life, an Antidote to Chaos"
// // }).then((user) => {
// //   console.log(JSON.stringify(user, undefined, 2));
// // }).catch((e) => {
// //   console.log(e.message);
// // })
  